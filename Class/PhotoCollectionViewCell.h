//
//  PhotoCollectionViewCell.h
//  PhotoUIDemo2
//
//  Created by reylen on 16/6/30.
//  Copyright © 2016年 reylen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImage *thumbnailImage;
@property (nonatomic, strong) UIImage *livePhotoBadgeImage;
@property (nonatomic, copy) NSString *representedAssetIdentifier;

@end
