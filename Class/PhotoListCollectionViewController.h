//
//  PhotoListCollectionViewController.h
//  PhotoUIDemo2
//
//  Created by reylen on 16/6/30.
//  Copyright © 2016年 reylen. All rights reserved.
//

#import <UIKit/UIKit.h>
@import PhotosUI;

@class PhotoListCollectionViewController;
@protocol PhotoListCollectionViewControllerDelegate <NSObject>

- (void) photoListCollectionViewController:(PhotoListCollectionViewController *) photoListCollectionViewController didSelectedImage:(UIImage *) image;
- (void) photoListCollectionViewControllerDidCancel:(PhotoListCollectionViewController *)photoListCollectionViewController;

@end

@interface PhotoListCollectionViewController : UICollectionViewController

@property (assign, nonatomic) id<PhotoListCollectionViewControllerDelegate>delegate;


@end
