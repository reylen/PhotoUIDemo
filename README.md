#PhotoUIDemo

本实例用到了PhotoUI框架，iOS8之后新增的框架。
本实例采用的是ARC模式。

具体使用方法，先将Class文件整个的移入项目中，然后再需要使用的地方调用，如：

```
- (IBAction)openPhotoLibrary:(id)sender {

    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    // 设置item尺寸
    layout.itemSize = CGSizeMake(80, 80);
    // 设置item之间的间隔
    layout.minimumInteritemSpacing = 0;
    // 设置行之间间隔
    layout.minimumLineSpacing = 20;
    // 设置组的内边距
    layout.sectionInset = UIEdgeInsetsMake(20, 0, 0, 0);
    PhotoListCollectionViewController* list = [[PhotoListCollectionViewController alloc] initWithCollectionViewLayout:layout];
    list.delegate = self;
    UINavigationController* navigationViewController = [[UINavigationController alloc] initWithRootViewController:list];
    [self presentViewController:navigationViewController animated:YES completion:nil];
}
```

由于选择图片的类中用到了delegate，所以需要实现委托，现在头部加上 
```
<PhotoListCollectionViewControllerDelegate>
```

```
#pragma mark - PhotoListCollectionViewControllerDelegate

- (void)photoListCollectionViewController:(PhotoListCollectionViewController *)photoListCollectionViewController didSelectedImage:(UIImage *)image {
    

    [photoListCollectionViewController dismissViewControllerAnimated:YES completion:^(void){
        self.imageView.image = image;
    }];
}

- (void)photoListCollectionViewControllerDidCancel:(PhotoListCollectionViewController *)photoListCollectionViewController {
    
    [photoListCollectionViewController dismissViewControllerAnimated:YES completion:nil];
}
```

具体使用可以根据项目进行修改