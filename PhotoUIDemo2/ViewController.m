//
//  ViewController.m
//  PhotoUIDemo2
//
//  Created by reylen on 16/6/29.
//  Copyright © 2016年 reylen. All rights reserved.
//

#import "ViewController.h"
#import "PhotoListCollectionViewController.h"
@import PhotosUI;

@interface ViewController ()<PhotoListCollectionViewControllerDelegate>

@property (strong, nonatomic) UIImageView* imageView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Photo" style:UIBarButtonItemStyleDone target:self action:@selector(openPhotoLibrary:)];
    self.imageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    self.imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.imageView.backgroundColor = [UIColor whiteColor];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:self.imageView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)openPhotoLibrary:(id)sender {

    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    // 设置item尺寸
    layout.itemSize = CGSizeMake(80, 80);
    // 设置item之间的间隔
    layout.minimumInteritemSpacing = 0;
    // 设置行之间间隔
    layout.minimumLineSpacing = 20;
    // 设置组的内边距
    layout.sectionInset = UIEdgeInsetsMake(20, 0, 0, 0);
    PhotoListCollectionViewController* list = [[PhotoListCollectionViewController alloc] initWithCollectionViewLayout:layout];
    list.delegate = self;
    UINavigationController* navigationViewController = [[UINavigationController alloc] initWithRootViewController:list];
    [self presentViewController:navigationViewController animated:YES completion:nil];
}

#pragma mark - PhotoListCollectionViewControllerDelegate

- (void)photoListCollectionViewController:(PhotoListCollectionViewController *)photoListCollectionViewController didSelectedImage:(UIImage *)image {
    

    [photoListCollectionViewController dismissViewControllerAnimated:YES completion:^(void){
        self.imageView.image = image;
    }];
}

- (void)photoListCollectionViewControllerDidCancel:(PhotoListCollectionViewController *)photoListCollectionViewController {
    
    [photoListCollectionViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
