//
//  AppDelegate.h
//  PhotoUIDemo2
//
//  Created by reylen on 16/6/29.
//  Copyright © 2016年 reylen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ViewController *viewController;


@end

